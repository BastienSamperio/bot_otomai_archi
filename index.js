const Discord = require('discord.js')
const bot = new Discord.Client()

// List of the emoji
var $sellEmoji = "💰"
var $exchangeEmoji = "🔁"
var $searchEmoji = "👀"

// List of the guild/channels
var $guildId = "407492327397916673"
var $oshimoChannel = "719555470200799292"
var $terraChannel = "720173346540748810"
var $herdegrizeChannel = "720165629553803286"

var $fraicheTestId = "744894482725732452"

// List of the archimonstre name

var $archimonstreName1 = "001 - Arachitik la Souffreteuse"
var $archimonstreName2 = "002 - Araknay la Galopante"
var $archimonstreName3 = "003 - Arakule la Revancharde"
var $archimonstreName4 = "004 - Boudalf le Blanc"
var $archimonstreName5 = "005 - Boulgourvil le Lointain"
var $archimonstreName6 = "006 - Chamchie le Difficile"
var $archimonstreName7 = "007 - Craraboss le Féérique"
var $archimonstreName8 = "008 - Larvonika l'Instrument"
var $archimonstreName9 = "009 - Mosketère le Dévoué"
var $archimonstreName10 = "010 - Pioufe la Maquillée"
var $archimonstreName11 = "011 - Pioukas la Plante"
var $archimonstreName12 = "012 - Pioulbrineur le Mercenaire"
var $archimonstreName13 = "013 - Pioulette la Coquine"
var $archimonstreName14 = "014 - Pioussokrim le Délétère"
var $archimonstreName15 = "015 - Pioustone le Problème"
var $archimonstreName16 = "016 - Pissdane l'Insipide"
var $archimonstreName17 = "017 - Sourizoto le Collant"
var $archimonstreName18 = "018 - Tofuldebeu l'Explosif"
var $archimonstreName19 = "019 - Tofumanchou l'Empereur"
var $archimonstreName20 = "020 - Tofurapin le Pétri"
var $archimonstreName21 = "021 - Bandapar l'Exclu"
var $archimonstreName22 = "022 - Bandson le Tonitruant"
var $archimonstreName23 = "023 - Boudur le Raide"
var $archimonstreName24 = "024 - Boufdégou le Refoulant"
var $archimonstreName25 = "025 - Bourdilleu le Social"
var $archimonstreName26 = "026 - Gelanal le Huileux"
var $archimonstreName27 = "027 - Geloliaine l'Aérien"
var $archimonstreName28 = "028 - Gobstiniais le Têtu"
var $archimonstreName29 = "029 - Grokosto le Bosco"
var $archimonstreName30 = "030 - Kolforthe l'Indécollable"
var $archimonstreName31 = "031 - Kwoanneur le Frimeur"
var $archimonstreName32 = "032 - Larchimaide la Poussée"
var $archimonstreName33 = "033 - Larvapstrè le Subjectif"
var $archimonstreName34 = "034 - Nodkoku le Trahi"
var $archimonstreName35 = "035 - Roz la Magicienne"
var $archimonstreName36 = "036 - Tiwa'Missou le Gateux"
var $archimonstreName37 = "037 - Tiwalpé le Dévêtu"
var $archimonstreName38 = "038 - Tiwoflan le Lâche"
var $archimonstreName39 = "039 - Tour le Vice"
var $archimonstreName40 = "040 - Trukul le Lent"
var $archimonstreName41 = "041 - Bi le Partageur"
var $archimonstreName42 = "042 - Bilvoezé le Bonimenteur"
var $archimonstreName43 = "043 - Bistou le Quêteur"
var $archimonstreName44 = "044 - Bistou le Rieur"
var $archimonstreName45 = "045 - Boostif l'Affamé"
var $archimonstreName46 = "046 - Bouflet le Puéril"
var $archimonstreName47 = "047 - Bulleur le Dormeur"
var $archimonstreName48 = "048 - Champayr le Disjoncté"
var $archimonstreName49 = "049 - Chevaustine le Reconstruit"
var $archimonstreName50 = "050 - Chonstip la Passagère"
var $archimonstreName51 = "051 - Citassaté le Service"
var $archimonstreName52 = "052 - Cromikay le Néophyte"
var $archimonstreName53 = "053 - Milipussien le Géant"
var $archimonstreName54 = "054 - Porfavor le Quémandeur"
var $archimonstreName55 = "055 - Preskapwal le Tendancieux"
var $archimonstreName56 = "056 - Sangria le Fruité"
var $archimonstreName57 = "057 - Tortenssia la Fleurie"
var $archimonstreName58 = "058 - Torthur la Lutte"
var $archimonstreName59 = "059 - Vampunor le Glacial"
var $archimonstreName60 = "060 - Wabbitud le Constant"
var $archimonstreName61 = "061 - Bambouské le Camouflé"
var $archimonstreName62 = "062 - Barchwork le Multicolore"
var $archimonstreName63 = "063 - Buldeflore le Pénétrant"
var $archimonstreName64 = "064 - Bwormage le Respectueux"
var $archimonstreName65 = "065 - Corpat le Vampire"
var $archimonstreName66 = "066 - Crognan le Barbare"
var $archimonstreName67 = "067 - Fourapin le Chaud"
var $archimonstreName68 = "068 - Maître Amboat le Moqueur"
var $archimonstreName69 = "069 - Milipatte la Griffe"
var $archimonstreName70 = "070 - Minoskour le Sauveur"
var $archimonstreName71 = "071 - Minsinistre l'Elu"
var $archimonstreName72 = "072 - Neufedur le Flottant"
var $archimonstreName73 = "073 - Pichakoté le Dégoûtant"
var $archimonstreName74 = "074 - Pichdourse le Puissant"
var $archimonstreName75 = "075 - Pichduitre le Totem"
var $archimonstreName76 = "076 - Pichtoire l'Erudit"
var $archimonstreName77 = "077 - Rostensyl la Cuisinière"
var $archimonstreName78 = "078 - Tortilleur le Coulé"
var $archimonstreName79 = "079 - Tortorak le Cornu"
var $archimonstreName80 = "080 - Wagnagnah le Sanglant"
var $archimonstreName81 = "081 - Abrakroc l'Edenté"
var $archimonstreName82 = "082 - Bworkasse le Dégoûtant"
var $archimonstreName83 = "083 - Chafalfer l'Optimiste"
var $archimonstreName84 = "084 - Chamdblé le Cultivé"
var $archimonstreName85 = "085 - Chamflay le Ballonné"
var $archimonstreName86 = "086 - Champayt l'Odorant"
var $archimonstreName87 = "087 - Crakmitaine le Faucheur"
var $archimonstreName88 = "088 - Craquetuss le Piquant"
var $archimonstreName89 = "089 - Draglida la Disparue"
var $archimonstreName90 = "090 - Garsim le Mort"
var $archimonstreName91 = "091 - Gelaviv le Glaçon"
var $archimonstreName92 = "092 - Kitsoupierre le Récipient"
var $archimonstreName93 = "093 - Krapahut le Randonneur"
var $archimonstreName94 = "094 - Mandalo l'Aqueuse"
var $archimonstreName95 = "095 - Ribibi le Cher"
var $archimonstreName96 = "096 - Scapé l'Epée"
var $archimonstreName97 = "097 - Scaramel le Fondant"
var $archimonstreName98 = "098 - Scarfayss le Balafré"
var $archimonstreName99 = "099 - Scarouarze l'Epopée"
var $archimonstreName100 = "100 - Watdogue le Bien Nommé"
var $archimonstreName101 = "101 - Abrakadnuzar"
var $archimonstreName102 = "102 - Blof l'Apathique"
var $archimonstreName103 = "103 - Bloporte le Veule"
var $archimonstreName104 = "104 - Blordur l'Infect"
var $archimonstreName105 = "105 - Blorie l'Assourdissante"
var $archimonstreName106 = "106 - Boombata le Garde"
var $archimonstreName107 = "107 - Bulsavon le Gonflé"
var $archimonstreName108 = "108 - Chafemal le Bagarreur"
var $archimonstreName109 = "109 - Chaffoin le Sournois"
var $archimonstreName110 = "110 - Crolnareff l'Exilé"
var $archimonstreName111 = "111 - Cruskof le Rustre"
var $archimonstreName112 = "112 - Crustensyl le Pragmatique"
var $archimonstreName113 = "113 - Crustterus l'Organique"
var $archimonstreName114 = "114 - Dragnoute l'Irascible"
var $archimonstreName115 = "115 - Farlon l'Enfant"
var $archimonstreName116 = "116 - Picht le Brioché"
var $archimonstreName117 = "117 - Radoutable le Craint"
var $archimonstreName118 = "118 - Scaratyn l'Huître"
var $archimonstreName119 = "119 - Scorpitène l'Enflammé"
var $archimonstreName120 = "120 - Tronkoneuz la Tranchante"
var $archimonstreName121 = "121 - Aboudbra le Porteur"
var $archimonstreName122 = "122 - Ameur la Laide"
var $archimonstreName123 = "123 - Arabord la Cruche"
var $archimonstreName124 = "124 - Arakozette l'Intrépide"
var $archimonstreName125 = "125 - Cavordemal le Sorcier"
var $archimonstreName126 = "126 - Chafmarcel le Fêtard"
var $archimonstreName127 = "127 - Chalan le Commerçant"
var $archimonstreName128 = "128 - Codenlgaz le Problème"
var $archimonstreName129 = "129 - Crusmeyer le Pervers"
var $archimonstreName130 = "130 - Forboyar l'Enigmatique"
var $archimonstreName131 = "131 - Ginsenk le Stimulant"
var $archimonstreName132 = "132 - Grandilok le Clameur"
var $archimonstreName133 = "133 - Kiroyal le Sirupeux"
var $archimonstreName134 = "134 - Koktèle le Secoué"
var $archimonstreName135 = "135 - Let le Rond"
var $archimonstreName136 = "136 - Nelvin le Boulet"
var $archimonstreName137 = "137 - Nipulnislip l'Exhibitionniste"
var $archimonstreName138 = "138 - Osuxion le Vampirique"
var $archimonstreName139 = "139 - Susbewl l'Hypocrite"
var $archimonstreName140 = "140 - Wokènrôl le Danseur"
var $archimonstreName141 = "141 - Abrakildas le Vénérable"
var $archimonstreName142 = "142 - Chafrit le Barbare"
var $archimonstreName143 = "143 - Chamitant le Dilettante"
var $archimonstreName144 = "144 - Cramikaz le Suicidaire"
var $archimonstreName145 = "145 - Craquetou le Fissuré"
var $archimonstreName146 = "146 - Doktopuss le Maléfique"
var $archimonstreName147 = "147 - Germinol l'Indigent"
var $archimonstreName148 = "148 - Kannibal le Lecteur"
var $archimonstreName149 = "149 - Kapota la Fraise"
var $archimonstreName150 = "150 - Koalastrof la Naturelle"
var $archimonstreName151 = "151 - Maître Onom le Régulier"
var $archimonstreName152 = "152 - Ouashouash l'Exubérant"
var $archimonstreName153 = "153 - Ouassébo l'Esthète"
var $archimonstreName154 = "154 - Ouature la Mobile"
var $archimonstreName155 = "155 - Palmiche le Serein"
var $archimonstreName156 = "156 - Palmiflette le Convivial"
var $archimonstreName157 = "157 - Palmito le Menteur"
var $archimonstreName158 = "158 - Pandimaensh l'Animateur"
var $archimonstreName159 = "159 - Pandimy le Contagieux"
var $archimonstreName160 = "160 - Sampi l'Eternel"
var $archimonstreName161 = "161 - Corboyard l'Enigmatique"
var $archimonstreName162 = "162 - Dragalgan l'Effervescent"
var $archimonstreName163 = "163 - Dragioli le Succulent"
var $archimonstreName164 = "164 - Dragsta le Détendu"
var $archimonstreName165 = "165 - Dragstik le Frustre"
var $archimonstreName166 = "166 - Dragstore le Généraliste"
var $archimonstreName167 = "167 - Dragtopaile l'Excavateur"
var $archimonstreName168 = "168 - Dragtula l'Ancien"
var $archimonstreName169 = "169 - Dragybuss le Sucré"
var $archimonstreName170 = "170 - Kannémik le Maigre"
var $archimonstreName171 = "171 - Kannisterik le Forcené"
var $archimonstreName172 = "172 - Kitsoudbra le Malodorant"
var $archimonstreName173 = "173 - Léopolnor le Barde"
var $archimonstreName174 = "174 - Ougaould le Parasite"
var $archimonstreName175 = "175 - Palmbytch la Bronzée"
var $archimonstreName176 = "176 - Pandanlagl la Saoule"
var $archimonstreName177 = "177 - Pandouille le Titubant"
var $archimonstreName178 = "178 - Ratlbol l'Aigri"
var $archimonstreName179 = "179 - Rauligo le Sale"
var $archimonstreName180 = "180 - Serpistule le Purulent"
var $archimonstreName181 = "181 - Barebourd le Comte"
var $archimonstreName182 = "182 - Caboume l'Artilleur"
var $archimonstreName183 = "183 - Chiendanlémin l'Illusionniste"
var $archimonstreName184 = "184 - Cooligane le Nevrosé"
var $archimonstreName185 = "185 - Dragkouine la Déguisée"
var $archimonstreName186 = "186 - Dragmoclaiss le Fataliste"
var $archimonstreName187 = "187 - Dragnostik le Sceptique"
var $archimonstreName188 = "188 - Dragstayr le Fonceur"
var $archimonstreName189 = "189 - Fandanleuil le Précis"
var $archimonstreName190 = "190 - Fanlabiz le Véloce"
var $archimonstreName191 = "191 - Fantoch le Pantin"
var $archimonstreName192 = "192 - Fantrask le Rêveur"
var $archimonstreName193 = "193 - Koakofrui le Confit"
var $archimonstreName194 = "194 - Koamaembair le Coulant"
var $archimonstreName195 = "195 - Koarmit la Batracienne"
var $archimonstreName196 = "196 - Koaskette la Chapelière"
var $archimonstreName197 = "197 - Nakuneuye le Borgne"
var $archimonstreName198 = "198 - Nerdeubeu le Flagellant"
var $archimonstreName199 = "199 - Ratéhaifaim le Professeur"
var $archimonstreName200 = "200 - Sparoket le Lanceur"
var $archimonstreName201 = "201 - Crathdogue le Cruel"
var $archimonstreName202 = "202 - Crok le Beau"
var $archimonstreName203 = "203 - Dragma le Bouillant"
var $archimonstreName204 = "204 - Dragoeth le Penseur"
var $archimonstreName205 = "205 - Dragonienne l'Econome"
var $archimonstreName206 = "206 - Dragoo le Cramoisi"
var $archimonstreName207 = "207 - Dragtonien le Malvoyant"
var $archimonstreName208 = "208 - Fanfancisco le Cosmopolite"
var $archimonstreName209 = "209 - Fanhnatur le Simple"
var $archimonstreName210 = "210 - Fanjipann le Sucré"
var $archimonstreName211 = "211 - Fanjo le Pilote"
var $archimonstreName212 = "212 - Fanlmyl l'Acuité"
var $archimonstreName213 = "213 - Guerrite le Veilleur"
var $archimonstreName214 = "214 - Kanasukr le Mielleux"
var $archimonstreName215 = "215 - Kido l'Âtre"
var $archimonstreName216 = "216 - Kilimanj'haro le Grimpeur"
var $archimonstreName217 = "217 - Mufguedin le Suprême"
var $archimonstreName218 = "218 - Muloufok l'Hilarant"
var $archimonstreName219 = "219 - Pangraive le Militant"
var $archimonstreName220 = "220 - Toufou le Benêt"
var $archimonstreName221 = "221 - Abrakanette l'Encapsulé"
var $archimonstreName222 = "222 - Abraklette le Fondant"
var $archimonstreName223 = "223 - Bambono le Divin"
var $archimonstreName224 = "224 - Bitoven le Musicien"
var $archimonstreName225 = "225 - Bramin le Bicéphale"
var $archimonstreName226 = "226 - Brouste l'Humiliant"
var $archimonstreName227 = "227 - Bulgig le Danseur"
var $archimonstreName228 = "228 - Diskord le Belliqueux"
var $archimonstreName229 = "229 - Dragdikal le Décisif"
var $archimonstreName230 = "230 - Dragobert le Monarque"
var $archimonstreName231 = "231 - Fossamoel le Juteux"
var $archimonstreName232 = "232 - Gloubibou le Gars"
var $archimonstreName233 = "233 - Kitsoupopulère le Généreux"
var $archimonstreName234 = "234 - Koamag'oel le Défiguré"
var $archimonstreName235 = "235 - Koasossyal le Psychopathe"
var $archimonstreName236 = "236 - Meuroup le Prêtre"
var $archimonstreName237 = "237 - Onihylis le Destructeur"
var $archimonstreName238 = "238 - Pékeutar le Tireur"
var $archimonstreName239 = "239 - Piradain le Pingre"
var $archimonstreName240 = "240 - Warkolad l'Etreinte"
var $archimonstreName241 = "241 - Abrakine le Sombre"
var $archimonstreName242 = "242 - Bouliver le Géant"
var $archimonstreName243 = "243 - Drageaufol la Joyeuse"
var $archimonstreName244 = "244 - Dragminster le Magicien"
var $archimonstreName245 = "245 - Dragtarus le Bellâtre"
var $archimonstreName246 = "246 - Drakolage le Tentateur"
var $archimonstreName247 = "247 - Draquetteur le Voleur"
var $archimonstreName248 = "248 - Ecorfé la Vive"
var $archimonstreName249 = "249 - Faufoll la Joyeuse"
var $archimonstreName250 = "250 - Floanna la Blonde"
var $archimonstreName251 = "251 - Gastroth la Contagieuse"
var $archimonstreName252 = "252 - Guerumoth le Collant"
var $archimonstreName253 = "253 - Kitsoufre l'Explosif"
var $archimonstreName254 = "254 - Koalaboi le Calorifère"
var $archimonstreName255 = "255 - Koalvissie le Chauve"
var $archimonstreName256 = "256 - Mamakomou l'Âge"
var $archimonstreName257 = "257 - Momikonos la Bandelette"
var $archimonstreName258 = "258 - Soryonara le Poli"
var $archimonstreName259 = "259 - Tourbiket le Virevoletant"
var $archimonstreName260 = "260 - Wara l'Amer"
var $archimonstreName261 = "261 - Abrinos le Clair"
var $archimonstreName262 = "262 - Chamiléro le Malchanceux"
var $archimonstreName263 = "263 - Chamoute le Duveteux"
var $archimonstreName264 = "264 - Champmé le Méchant"
var $archimonstreName265 = "265 - Champolyon le Polyglotte"
var $archimonstreName266 = "266 - Champoul l'Illuminé"
var $archimonstreName267 = "267 - Don Kizoth l'Obstiné"
var $archimonstreName268 = "268 - Fandouich l'Hautain"
var $archimonstreName269 = "269 - Fanhopruno le Gourmet"
var $archimonstreName270 = "270 - Fansissla l'Âne"
var $archimonstreName271 = "271 - Fanstatik l'Etonnant"
var $archimonstreName272 = "272 - Kaskapointhe la Couverte"
var $archimonstreName273 = "273 - Mahoku le Botté"
var $archimonstreName274 = "274 - Maître Koantik le Théoricien"
var $archimonstreName275 = "275 - Poolopo la Traditionnelle"
var $archimonstreName276 = "276 - Seripoth l'Ennemi"
var $archimonstreName277 = "277 - Toutouf le Velu"
var $archimonstreName278 = "278 - Tromplamor le Survivant"
var $archimonstreName279 = "279 - Trooyé l'Oxydé"
var $archimonstreName280 = "280 - YokaiKoral le Duel"
var $archimonstreName281 = "281 - Fanburn le Viril"
var $archimonstreName282 = "282 - Fanlagoel le Comique"
var $archimonstreName283 = "283 - Fansiss la Brêle"
var $archimonstreName284 = "284 - Fantassein le Soldat"
var $archimonstreName285 = "285 - Pandive le Végétarien"
var $archimonstreName286 = "286 - Roy le Merlin"

// List of the Oshimo message
var $messageOshimo1 = "719855722032005130"
var $messageOshimo2 = "719855743997575189"
var $messageOshimo3 = "719855764889665586"
var $messageOshimo4 = "719855786070900777"
var $messageOshimo5 = "719855804634890291"
var $messageOshimo6 = "719855832359239763"
var $messageOshimo7 = "719855856417636382"
var $messageOshimo8 = "719855878420955206"
var $messageOshimo9 = "719855897664552992"
var $messageOshimo10 = "719855917394558996"
var $messageOshimo11 = "719855938466480210"
var $messageOshimo12 = "719855966044291082"
var $messageOshimo13 = "719855991038148649"
var $messageOshimo14 = "719856011401494558"
var $messageOshimo15 = "719856036281974834"
var $messageOshimo16 = "719856054762209291"
var $messageOshimo17 = "719856078325809224"
var $messageOshimo18 = "719856103659274310"
var $messageOshimo19 = "719856125029253222"
var $messageOshimo20 = "719856148320354354"
var $messageOshimo21 = "719856247142088754"
var $messageOshimo22 = "719856266884677653"
var $messageOshimo23 = "719856291002056746"
var $messageOshimo24 = "719856324787044402"
var $messageOshimo25 = "719856364024889345"
var $messageOshimo26 = "719856390772097125"
var $messageOshimo27 = "719856415220695066"
var $messageOshimo28 = "719856446564466808"
var $messageOshimo29 = "719856474083557418"
var $messageOshimo30 = "719856506702659584"
var $messageOshimo31 = "719856531746717766"
var $messageOshimo32 = "719856551044579350"
var $messageOshimo33 = "719856580656496691"
var $messageOshimo34 = "719856599241588817"
var $messageOshimo35 = "719856618530930808"
var $messageOshimo36 = "719856642887254057"
var $messageOshimo37 = "719856661056979025"
var $messageOshimo38 = "719856678094372924"
var $messageOshimo39 = "719856698243809333"
var $messageOshimo40 = "719856724173127730"
var $messageOshimo41 = "719856934773063790"
var $messageOshimo42 = "719856976871555152"
var $messageOshimo43 = "719857039945367553"
var $messageOshimo44 = "719857086187438111"
var $messageOshimo45 = "719857145658736691"
var $messageOshimo46 = "719857202462064650"
var $messageOshimo47 = "719857247924125717"
var $messageOshimo48 = "719857288277393440"
var $messageOshimo49 = "719857329419321355"
var $messageOshimo50 = "719857369710067723"
var $messageOshimo51 = "719857413003542538"
var $messageOshimo52 = "719858306549547098"
var $messageOshimo53 = "719858367165890600"
var $messageOshimo54 = "719858422174056578"
var $messageOshimo55 = "719858464695910451"
var $messageOshimo56 = "719858528747126826"
var $messageOshimo57 = "719858586104233994"
var $messageOshimo58 = "719858632375664731"
var $messageOshimo59 = "719858711102881903"
var $messageOshimo60 = "719858718153506827"
var $messageOshimo61 = "719859409076879433"
var $messageOshimo62 = "719859468162302072"
var $messageOshimo63 = "719859503604039721"
var $messageOshimo64 = "719859539368738917"
var $messageOshimo65 = "719859577809535007"
var $messageOshimo66 = "719859618968502354"
var $messageOshimo67 = "719859659397136535"
var $messageOshimo68 = "719859697900978228"
var $messageOshimo69 = "719859739390902302"
var $messageOshimo70 = "719859779421470750"
var $messageOshimo71 = "719859817035858063"
var $messageOshimo72 = "719859851286675536"
var $messageOshimo73 = "719859891660914690"
var $messageOshimo74 = "719859932471492659"
var $messageOshimo75 = "719859986020302908"
var $messageOshimo76 = "719860035856891904"
var $messageOshimo77 = "719860090127253535"
var $messageOshimo78 = "719860130107359293"
var $messageOshimo79 = "719860169273770004"
var $messageOshimo80 = "719860217139036182"
var $messageOshimo81 = "719882191093497917"
var $messageOshimo82 = "719882210840412250"
var $messageOshimo83 = "719882246407979178"
var $messageOshimo84 = "719882285159022595"
var $messageOshimo85 = "719882330948239480"
var $messageOshimo86 = "719882363429191771"
var $messageOshimo87 = "719882401576124456"
var $messageOshimo88 = "719882435319431219"
var $messageOshimo89 = "719882469658329089"
var $messageOshimo90 = "719882504676573244"
var $messageOshimo91 = "719882541565214753"
var $messageOshimo92 = "719882576281731102"
var $messageOshimo93 = "719882606023409735"
var $messageOshimo94 = "719882650428375040"
var $messageOshimo95 = "719882683894988860"
var $messageOshimo96 = "719882718414110773"
var $messageOshimo97 = "719882752085852211"
var $messageOshimo98 = "719882782675042355"
var $messageOshimo99 = "719882814937628703"
var $messageOshimo100 = "719882848387072040"
var $messageOshimo101 = "719885199957032981"
var $messageOshimo102 = "719885238511206462"
var $messageOshimo103 = "719885271692476467"
var $messageOshimo104 = "719885321776529409"
var $messageOshimo105 = "719885355855118367"
var $messageOshimo106 = "719885389896351854"
var $messageOshimo107 = "719885430287499285"
var $messageOshimo108 = "719885466031357992"
var $messageOshimo109 = "719885500105752620"
var $messageOshimo110 = "719885550882127924"
var $messageOshimo111 = "719885592640618516"
var $messageOshimo112 = "719885626752630937"
var $messageOshimo113 = "719885671208058950"
var $messageOshimo114 = "719885711888613424"
var $messageOshimo115 = "719885748643430492"
var $messageOshimo116 = "719885790573887620"
var $messageOshimo117 = "719885829048369203"
var $messageOshimo118 = "719885869506625576"
var $messageOshimo119 = "719885904218554438"
var $messageOshimo120 = "719885939723468893"
var $messageOshimo121 = "719886715896201337"
var $messageOshimo122 = "719886746397179904"
var $messageOshimo123 = "719886778462502974"
var $messageOshimo124 = "719886810138017792"
var $messageOshimo125 = "719886843792850945"
var $messageOshimo126 = "719886889913417748"
var $messageOshimo127 = "719886924789055539"
var $messageOshimo128 = "719886965675130880"
var $messageOshimo129 = "719887002455244811"
var $messageOshimo130 = "719887042405990410"
var $messageOshimo131 = "719887076534779906"
var $messageOshimo132 = "719887118138081330"
var $messageOshimo133 = "719887158957047849"
var $messageOshimo134 = "719887193321242627"
var $messageOshimo135 = "719887234672885860"
var $messageOshimo136 = "719887272568422470"
var $messageOshimo137 = "719887307699912744"
var $messageOshimo138 = "719887345075355729"
var $messageOshimo139 = "719887387081310258"
var $messageOshimo140 = "719887423202656377"
var $messageOshimo141 = "719888832165576764"
var $messageOshimo142 = "719888866835824651"
var $messageOshimo143 = "719888899773562881"
var $messageOshimo144 = "719888934900990012"
var $messageOshimo145 = "719888980530954262"
var $messageOshimo146 = "719889021668425749"
var $messageOshimo147 = "719889063355613194"
var $messageOshimo148 = "719889107232358460"
var $messageOshimo149 = "719889144867848252"
var $messageOshimo150 = "719889178917339197"
var $messageOshimo151 = "719889215504252989"
var $messageOshimo152 = "719889256889450527"
var $messageOshimo153 = "719889296420765727"
var $messageOshimo154 = "719889334278553690"
var $messageOshimo155 = "719889372702441603"
var $messageOshimo156 = "719889410501640193"
var $messageOshimo157 = "719889447985872968"
var $messageOshimo158 = "719889481456418926"
var $messageOshimo159 = "719889517145882625"
var $messageOshimo160 = "719889549664321566"
var $messageOshimo161 = "719890292278558812"
var $messageOshimo162 = "719890327770759525"
var $messageOshimo163 = "719890362726088724"
var $messageOshimo164 = "719890398394318920"
var $messageOshimo165 = "719890429511991386"
var $messageOshimo166 = "719890465696120903"
var $messageOshimo167 = "719890500072505454"
var $messageOshimo168 = "719890531936632904"
var $messageOshimo169 = "719890565834997851"
var $messageOshimo170 = "719890599993409666"
var $messageOshimo171 = "719890638463827988"
var $messageOshimo172 = "719890672852795404"
var $messageOshimo173 = "719890708860895333"
var $messageOshimo174 = "719890743522754580"
var $messageOshimo175 = "719890775659249704"
var $messageOshimo176 = "719890813299195974"
var $messageOshimo177 = "719890851995582486"
var $messageOshimo178 = "719890889291595878"
var $messageOshimo179 = "719890933419606036"
var $messageOshimo180 = "719890966353543168"
var $messageOshimo181 = "719903780031823896"
var $messageOshimo182 = "719903819953340426"
var $messageOshimo183 = "719903852731695194"
var $messageOshimo184 = "719903885736673322"
var $messageOshimo185 = "719903922097094766"
var $messageOshimo186 = "719903962488373248"
var $messageOshimo187 = "719903997850419330"
var $messageOshimo188 = "719904034818883584"
var $messageOshimo189 = "719904066020573184"
var $messageOshimo190 = "719904098710847519"
var $messageOshimo191 = "719904133485953094"
var $messageOshimo192 = "719904167736639560"
var $messageOshimo193 = "719904206449934477"
var $messageOshimo194 = "719904237277937684"
var $messageOshimo195 = "719904269456900247"
var $messageOshimo196 = "719904308551876639"
var $messageOshimo197 = "719904348703948821"
var $messageOshimo198 = "719904405805334548"
var $messageOshimo199 = "719904442668941332"
var $messageOshimo200 = "719904482309177375"
var $messageOshimo201 = "719919713127235674"
var $messageOshimo202 = "719919734539288646"
var $messageOshimo203 = "719919759889531020"
var $messageOshimo204 = "719919786938859521"
var $messageOshimo205 = "719919816936521778"
var $messageOshimo206 = "719919836599156746"
var $messageOshimo207 = "719919854588788757"
var $messageOshimo208 = "719919879360086116"
var $messageOshimo209 = "719919897953435658"
var $messageOshimo210 = "719919919042396230"
var $messageOshimo211 = "719919938155839620"
var $messageOshimo212 = "719919958422978620"
var $messageOshimo213 = "719919987262881823"
var $messageOshimo214 = "719920004816175214"
var $messageOshimo215 = "719920020741816321"
var $messageOshimo216 = "719920036570988635"
var $messageOshimo217 = "719920049867063387"
var $messageOshimo218 = "719920064391807087"
var $messageOshimo219 = "719920167311638543"
var $messageOshimo220 = "719920205039534111"
var $messageOshimo221 = "719920288900579399"
var $messageOshimo222 = "719920352930562088"
var $messageOshimo223 = "719920383440191498"
var $messageOshimo224 = "719920471902126190"
var $messageOshimo225 = "719920537765150860"
var $messageOshimo226 = "719920586108698636"
var $messageOshimo227 = "719920622704132177"
var $messageOshimo228 = "719920676416258128"
var $messageOshimo229 = "719920727368794287"
var $messageOshimo230 = "719920775758610433"
var $messageOshimo231 = "719920816929898536"
var $messageOshimo232 = "719920858621018223"
var $messageOshimo233 = "719920897816920104"
var $messageOshimo234 = "719921295474557049"
var $messageOshimo235 = "719921333697249404"
var $messageOshimo236 = "719921372414869535"
var $messageOshimo237 = "719921413175115838"
var $messageOshimo238 = "719921451334893668"
var $messageOshimo239 = "719921517730725968"
var $messageOshimo240 = "719921552262692945"
var $messageOshimo241 = "719927030493347871"
var $messageOshimo242 = "719927057894604823"
var $messageOshimo243 = "719927082611769414"
var $messageOshimo244 = "719927114929012777"
var $messageOshimo245 = "719927151708602398"
var $messageOshimo246 = "719927178527244300"
var $messageOshimo247 = "719927213528711198"
var $messageOshimo248 = "719927234449768448"
var $messageOshimo249 = "719927259162476572"
var $messageOshimo250 = "719927285834055742"
var $messageOshimo251 = "719927314728747059"
var $messageOshimo252 = "719927335826096128"
var $messageOshimo253 = "719927362862448681"
var $messageOshimo254 = "719927388787703879"
var $messageOshimo255 = "719927418160283688"
var $messageOshimo256 = "719927442889900163"
var $messageOshimo257 = "719927472811933719"
var $messageOshimo258 = "719927493393645578"
var $messageOshimo259 = "719927511223369798"
var $messageOshimo260 = "719927531930779769"
var $messageOshimo261 = "719927577451692115"
var $messageOshimo262 = "719927599132049491"
var $messageOshimo263 = "719927626210344974"
var $messageOshimo264 = "719927647270076416"
var $messageOshimo265 = "719927667654393967"
var $messageOshimo266 = "719927687224754246"
var $messageOshimo267 = "719927710914183358"
var $messageOshimo268 = "719927730996641874"
var $messageOshimo269 = "719927751481753630"
var $messageOshimo270 = "719927770829946910"
var $messageOshimo271 = "719927795786186863"
var $messageOshimo272 = "719927820377129021"
var $messageOshimo273 = "719927843529949194"
var $messageOshimo274 = "719927868389457940"
var $messageOshimo275 = "719927889247731725"
var $messageOshimo276 = "719927912094105751"
var $messageOshimo277 = "719927934336499823"
var $messageOshimo278 = "719927955047841862"
var $messageOshimo279 = "719927973188337825"
var $messageOshimo280 = "719927993966788680"
var $messageOshimo281 = "720164733520576582"
var $messageOshimo282 = "720164779913642045"
var $messageOshimo283 = "720164817763172383"
var $messageOshimo284 = "720164854559670293"
var $messageOshimo285 = "720164892690219018"
var $messageOshimo286 = "720164928547323974"

var $messageTerra1 = "720173416077983754"
var $messageTerra2 = "720173432766988318"
var $messageTerra3 = "720173445094178828"
var $messageTerra4 = "720173457475764234"
var $messageTerra5 = "720173471635603467"
var $messageTerra6 = "720173483908399145"
var $messageTerra7 = "720173496432459847"
var $messageTerra8 = "720173509548048445"
var $messageTerra9 = "720173527084302396"
var $messageTerra10 = "720173541097603172"
var $messageTerra11 = "720173557644132372"
var $messageTerra12 = "720173574488588319"
var $messageTerra13 = "720173592310186026"
var $messageTerra14 = "720173610458808360"
var $messageTerra15 = "720173625558433835"
var $messageTerra16 = "720173643270979644"
var $messageTerra17 = "720173663382667285"
var $messageTerra18 = "720173677533986857"
var $messageTerra19 = "720173692621029416"
var $messageTerra20 = "720173711591866489"
var $messageTerra21 = "720174337864368159"
var $messageTerra22 = "720174352623992912"
var $messageTerra23 = "720174365769072740"
var $messageTerra24 = "720174380738674688"
var $messageTerra25 = "720174392864407615"
var $messageTerra26 = "720174406504153168"
var $messageTerra27 = "720174421964488734"
var $messageTerra28 = "720174436547821618"
var $messageTerra29 = "720174450053742602"
var $messageTerra30 = "720174466940010497"
var $messageTerra31 = "720174479703015475"
var $messageTerra32 = "720174496186630235"
var $messageTerra33 = "720174513764958208"
var $messageTerra34 = "720174530147909644"
var $messageTerra35 = "720174544798613506"
var $messageTerra36 = "720174562255306772"
var $messageTerra37 = "720174579359678546"
var $messageTerra38 = "720174592148373504"
var $messageTerra39 = "720174605846708284"
var $messageTerra40 = "720174623681150976"
var $messageTerra41 = "720174652491694092"
var $messageTerra42 = "720174667549376552"
var $messageTerra43 = "720174684775120986"
var $messageTerra44 = "720174700529188906"
var $messageTerra45 = "720174714974109706"
var $messageTerra46 = "720174729008381992"
var $messageTerra47 = "720174747232501771"
var $messageTerra48 = "720174761258516570"
var $messageTerra49 = "720174779813986384"
var $messageTerra50 = "720174808133926913"
var $messageTerra51 = "720174821878661140"
var $messageTerra52 = "720174833781964821"
var $messageTerra53 = "720174847472435271"
var $messageTerra54 = "720174859736317963"
var $messageTerra55 = "720174872608899122"
var $messageTerra56 = "720174886592708769"
var $messageTerra57 = "720174899431211068"
var $messageTerra58 = "720174912383221832"
var $messageTerra59 = "720174929924063273"
var $messageTerra60 = "720174943882706974"
var $messageTerra61 = "720174972257042493"
var $messageTerra62 = "720174988379947029"
var $messageTerra63 = "720175007443189780"
var $messageTerra64 = "720175020541739070"
var $messageTerra65 = "720175034143866881"
var $messageTerra66 = "720175034143866881"
var $messageTerra67 = "720175064263426138"
var $messageTerra68 = "720175081342500933"
var $messageTerra69 = "720175097389908013"
var $messageTerra70 = "720175125583888466"
var $messageTerra71 = "720175140226334760"
var $messageTerra72 = "720175158152790016"
var $messageTerra73 = "720175174271631410"
var $messageTerra74 = "720175190314582136"
var $messageTerra75 = "720175203375775796"
var $messageTerra76 = "720175220735868988"
var $messageTerra77 = "720175236015980590"
var $messageTerra78 = "720175249471176704"
var $messageTerra79 = "720175265053147156"
var $messageTerra80 = "720175278273331210"
var $messageTerra81 = "720175310749958164"
var $messageTerra82 = "720175323735523378"
var $messageTerra83 = "720175341112524810"
var $messageTerra84 = "720175354685292554"
var $messageTerra85 = "720175368505655326"
var $messageTerra86 = "720175382354984971"
var $messageTerra87 = "720175394577448983"
var $messageTerra88 = "720175406698856570"
var $messageTerra89 = "720175423404769292"
var $messageTerra90 = "720175436427952139"
var $messageTerra91 = "720175448402690068"
var $messageTerra92 = "720175462868844614"
var $messageTerra93 = "720175482175356968"
var $messageTerra94 = "720175499048910979"
var $messageTerra95 = "720175513112543252"
var $messageTerra96 = "720175534113554484"
var $messageTerra97 = "720175547463762002"
var $messageTerra98 = "720175560432812072"
var $messageTerra99 = "720175575746084894"
var $messageTerra100 = "720175590803767416"
var $messageTerra101 = "720175621598216213"
var $messageTerra102 = "720175633006854165"
var $messageTerra103 = "720175645027729408"
var $messageTerra104 = "720175657388212285"
var $messageTerra105 = "720175671296393236"
var $messageTerra106 = "720175697838080023"
var $messageTerra107 = "720175711872352337"
var $messageTerra108 = "720175728976461866"
var $messageTerra109 = "720175743342215260"
var $messageTerra110 = "720175754905649214"
var $messageTerra111 = "720175767098753055"
var $messageTerra112 = "720175779484270612"
var $messageTerra113 = "720175797222113301"
var $messageTerra114 = "720175817195257936"
var $messageTerra115 = "720175834639499348"
var $messageTerra116 = "720175847452966962"
var $messageTerra117 = "720175860841447434"
var $messageTerra118 = "720175874133065739"
var $messageTerra119 = "720175890369347625"
var $messageTerra120 = "720175904005029981"
var $messageTerra121 = "720175935969820693"
var $messageTerra122 = "720175952939974697"
var $messageTerra123 = "720175966952882187"
var $messageTerra124 = "720175981708705822"
var $messageTerra125 = "720175995860287489"
var $messageTerra126 = "720176009718005790"
var $messageTerra127 = "720176026294026296"
var $messageTerra128 = "720176040827158568"
var $messageTerra129 = "720176056501272577"
var $messageTerra130 = "720176070153732130"
var $messageTerra131 = "720176082845827084"
var $messageTerra132 = "720176097869692940"
var $messageTerra133 = "720176112801677342"
var $messageTerra134 = "720176130979790909"
var $messageTerra135 = "720176145236099173"
var $messageTerra136 = "720176169009545267"
var $messageTerra137 = "720176187313225779"
var $messageTerra138 = "720176201230188564"
var $messageTerra139 = "720176213875884054"
var $messageTerra140 = "720176231257079819"
var $messageTerra141 = "720176275834011720"
var $messageTerra142 = "720176289423818792"
var $messageTerra143 = "720176302635614251"
var $messageTerra144 = "720176314493042689"
var $messageTerra145 = "720176333203963974"
var $messageTerra146 = "720176345442811914"
var $messageTerra147 = "720176358642155562"
var $messageTerra148 = "720176374064742430"
var $messageTerra149 = "720176387805413467"
var $messageTerra150 = "720176421523292211"
var $messageTerra151 = "720176438220947499"
var $messageTerra152 = "720176451961487362"
var $messageTerra153 = "720176468876984342"
var $messageTerra154 = "720176486539198504"
var $messageTerra155 = "720176500619477034"
var $messageTerra156 = "720176514586378280"
var $messageTerra157 = "720176533087453204"
var $messageTerra158 = "720176548199792640"
var $messageTerra159 = "720176561034362901"
var $messageTerra160 = "720176578738257941"
var $messageTerra161 = "720176673722728458"
var $messageTerra162 = "720176686456373328"
var $messageTerra163 = "720176699454783508"
var $messageTerra164 = "720176716596904006"
var $messageTerra165 = "720176730735902722"
var $messageTerra166 = "720176746649092207"
var $messageTerra167 = "720176759554965565"
var $messageTerra168 = "720176771906928690"
var $messageTerra169 = "720176785236426862"
var $messageTerra170 = "720176800524927048"
var $messageTerra171 = "720176816425402379"
var $messageTerra172 = "720176828790341663"
var $messageTerra173 = "720176842157326378"
var $messageTerra174 = "720176854748889098"
var $messageTerra175 = "720176871949729804"
var $messageTerra176 = "720176889012027392"
var $messageTerra177 = "720176904329494538"
var $messageTerra178 = "720176918460235806"
var $messageTerra179 = "720176936198078554"
var $messageTerra180 = "720176952538824736"
var $messageTerra181 = "720176985590071296"
var $messageTerra182 = "720176999276085254"
var $messageTerra183 = "720177015260446731"
var $messageTerra184 = "720177027805872169"
var $messageTerra185 = "720177040355229717"
var $messageTerra186 = "720177055852920842"
var $messageTerra187 = "720177071590211584"
var $messageTerra188 = "720177087717048350"
var $messageTerra189 = "720177102732656670"
var $messageTerra190 = "720177116632842250"
var $messageTerra191 = "720177132822593547"
var $messageTerra192 = "720177145938313236"
var $messageTerra193 = "720177160626896907"
var $messageTerra194 = "720177178452426792"
var $messageTerra195 = "720177195439620128"
var $messageTerra196 = "720177209867894814"
var $messageTerra197 = "720177226955620483"
var $messageTerra198 = "720177244353331270"
var $messageTerra199 = "720177259662802956"
var $messageTerra200 = "720177277631201304"
var $messageTerra201 = "720177308434038854"
var $messageTerra202 = "720177323197857852"
var $messageTerra203 = "720177340495167488"
var $messageTerra204 = "720177355351654451"
var $messageTerra205 = "720177375559680020"
var $messageTerra206 = "720177395075776544"
var $messageTerra207 = "720177409932001331"
var $messageTerra208 = "720177428558774303"
var $messageTerra209 = "720177445814403134"
var $messageTerra210 = "720177461500837929"
var $messageTerra211 = "720177481608331284"
var $messageTerra212 = "720177497739886655"
var $messageTerra213 = "720177513602613359"
var $messageTerra214 = "720177532703604797"
var $messageTerra215 = "720177548142575707"
var $messageTerra216 = "720177561501433858"
var $messageTerra217 = "720177579503648798"
var $messageTerra218 = "720177595588673567"
var $messageTerra219 = "720177613318127697"
var $messageTerra220 = "720177628384067586"
var $messageTerra221 = "720177659451146251"
var $messageTerra222 = "720177675867783248"
var $messageTerra223 = "720177691281719357"
var $messageTerra224 = "720177705383100416"
var $messageTerra225 = "720177723250835466"
var $messageTerra226 = "720177736798437487"
var $messageTerra227 = "720177753793757185"
var $messageTerra228 = "720177769786638337"
var $messageTerra229 = "720177788224536576"
var $messageTerra230 = "720177803173036072"
var $messageTerra231 = "720177816112595006"
var $messageTerra232 = "720177835884675102"
var $messageTerra233 = "720177851688681493"
var $messageTerra234 = "720177868340068363"
var $messageTerra235 = "720177886417518638"
var $messageTerra236 = "720177903115173958"
var $messageTerra237 = "720177917858152451"
var $messageTerra238 = "720177936103374850"
var $messageTerra239 = "720177955568877680"
var $messageTerra240 = "720177970899320902"
var $messageTerra241 = "720178002352406569"
var $messageTerra242 = "720178023978106930"
var $messageTerra243 = "720178038901440577"
var $messageTerra244 = "720178051073441832"
var $messageTerra245 = "720178064780427295"
var $messageTerra246 = "720178077757472820"
var $messageTerra247 = "720178095675670610"
var $messageTerra248 = "720178109151838228"
var $messageTerra249 = "720178121684287539"
var $messageTerra250 = "720178137748734002"
var $messageTerra251 = "720178154655711262"
var $messageTerra252 = "720178171437383691"
var $messageTerra253 = "720178184238268427"
var $messageTerra254 = "720178199400677405"
var $messageTerra255 = "720178215024328704"
var $messageTerra256 = "720178231164272691"
var $messageTerra257 = "720178247643430943"
var $messageTerra258 = "720178266580844556"
var $messageTerra259 = "720178285363068938"
var $messageTerra260 = "720178300676341801"
var $messageTerra261 = "720178331412070441"
var $messageTerra262 = "720178346570547231"
var $messageTerra263 = "720178361871106080"
var $messageTerra264 = "720178375938801754"
var $messageTerra265 = "720178392552439808"
var $messageTerra266 = "720178409791291393"
var $messageTerra267 = "720178424529813545"
var $messageTerra268 = "720178439977435148"
var $messageTerra269 = "720178457891569695"
var $messageTerra270 = "720178472948990003"
var $messageTerra271 = "720178486949445634"
var $messageTerra272 = "720178500061102091"
var $messageTerra273 = "720178517643624519"
var $messageTerra274 = "720178536706474064"
var $messageTerra275 = "720178551965614092"
var $messageTerra276 = "720178564628217907"
var $messageTerra277 = "720178578301517824"
var $messageTerra278 = "720178592255967253"
var $messageTerra279 = "720178606923317249"
var $messageTerra280 = "720178622660345916"
var $messageTerra281 = "720178654495375430"
var $messageTerra282 = "720178668034326568"
var $messageTerra283 = "720178683469365299"
var $messageTerra284 = "720178700020351027"
var $messageTerra285 = "720178712838144081"
var $messageTerra286 = "720178728797208636"


// Name of archimonstre

bot.on('message', function (message) {
    if (message.content === 'qui es-tu?') {
        message.channel.send("Je m'apelle Otomaï et je suis là pour vous aider dans la gestion de vos archimonstres")
    }

    if (message.content === 'hey') {
        for (let i = 1; i < 3; i++) {
            var current = '$archimonstreName' + i
            message.channel.send(eval(current))

        }
    }

    // Oshimo archimonstres commandes
    for (let i = 1; i < 287; i++) {
        if (message.content === '?os-' + i) {
            const ExchangeListEmbed = new Discord.MessageEmbed()
            var currentArchimonstre = '$archimonstreName' + i
            var currentMessage = '$messageOshimo' + i
            ExchangeListEmbed.setTitle("Les joueurs pouvant vous échanger l'archimonstre" + eval(currentArchimonstre) + " sont :")
            var $countUserExchange = 0
            bot.guilds.cache.get($guildId).channels.cache.get($oshimoChannel).messages.fetch(eval(currentMessage))
                .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $exchangeEmoji).users.fetch()
                    .then(users => users.forEach(function (currentUserExchange, index, currentUsersExchange) {
                        if (currentUserExchange.id == $fraicheTestId) {
                        } else {
                            if ($countUserExchange === 0) {
                                ExchangeListEmbed.setDescription("<@" + currentUserExchange.id + "> " + currentUserExchange.tag + ('\n'));
                            } else {
                                ExchangeListEmbed.setDescription(ExchangeListEmbed.description + "<@" + currentUserExchange.id + "> " + currentUserExchange.tag + ('\n'));
                            }
                        }
                        $countUserExchange++
                        $countTotal = 0
                        currentUsersExchange.forEach(userCurrent => {
                            $countTotal++
                        });
                        if ($countUserExchange == $countTotal) {
                            message.channel.send(ExchangeListEmbed);
                        }
                    }, this))).catch(console.error);
            var $countUserSell = 0
            const SellListEmbed = new Discord.MessageEmbed()
            SellListEmbed.setTitle("Les joueurs pouvant vous vendre l'archimonstre" + eval(currentArchimonstre) + " sont :")
            bot.guilds.cache.get($guildId).channels.cache.get($oshimoChannel).messages.fetch(eval(currentMessage))
                .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $sellEmoji).users.fetch()
                    .then(users => users.forEach(function (currentUserSell, index, currentUsersSell) {
                        if (currentUserSell.id == $fraicheTestId) {
                        } else {
                            if ($countUserSell === 0) {
                                SellListEmbed.setDescription("<@" + currentUserSell.id + "> " + currentUserSell.tag + ('\n'));
                            } else {
                                SellListEmbed.setDescription(SellListEmbed.description + "<@" + currentUserSell.id + "> " + currentUserSell.tag + ('\n'));
                            }
                        }
                        $countUserSell++
                        $countTotal = 0
                        currentUsersSell.forEach(userCurrent => {
                            $countTotal++
                        });
                        if ($countUserSell == $countTotal) {
                            message.channel.send(SellListEmbed);
                        }
                    }))).catch(console.error);
        }

    }

    // Terra Cogita archimonstre commandes 

    for (let i = 1; i < 287; i++) {
        if (message.content === '?tc-' + i) {
            const ExchangeListEmbed = new Discord.MessageEmbed()
            var currentArchimonstre = '$archimonstreName' + i
            var currentMessage = '$messageTerra' + i
            ExchangeListEmbed.setTitle("Les joueurs pouvant vous échanger l'archimonstre" + eval(currentArchimonstre) + " sont :")
            var $countUserExchange = 0
            bot.guilds.cache.get($guildId).channels.cache.get($terraChannel).messages.fetch(eval(currentMessage))
                .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $exchangeEmoji).users.fetch()
                    .then(users => users.forEach(function (currentUserExchange, index, currentUsersExchange) {
                        if (currentUserExchange.id == $fraicheTestId) {
                        } else {
                            if ($countUserExchange === 0) {
                                ExchangeListEmbed.setDescription("<@" + currentUserExchange.id + "> " + currentUserExchange.tag + ('\n'));
                            } else {
                                ExchangeListEmbed.setDescription(ExchangeListEmbed.description + "<@" + currentUserExchange.id + "> " + currentUserExchange.tag + ('\n'));
                            }
                        }
                        $countUserExchange++
                        $countTotal = 0
                        currentUsersExchange.forEach(userCurrent => {
                            $countTotal++
                        });
                        if ($countUserExchange == $countTotal) {
                            message.channel.send(ExchangeListEmbed);
                        }
                    }, this))).catch(console.error);
            var $countUserSell = 0
            const SellListEmbed = new Discord.MessageEmbed()
            SellListEmbed.setTitle("Les joueurs pouvant vous vendre l'archimonstre" + eval(currentArchimonstre) + " sont :")
            bot.guilds.cache.get($guildId).channels.cache.get($terraChannel).messages.fetch(eval(currentMessage))
                .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $sellEmoji).users.fetch()
                    .then(users => users.forEach(function (currentUserSell, index, currentUsersSell) {
                        if (currentUserSell.id == $fraicheTestId) {
                        } else {
                            if ($countUserSell === 0) {
                                SellListEmbed.setDescription("<@" + currentUserSell.id + "> " + currentUserSell.tag + ('\n'));
                            } else {
                                SellListEmbed.setDescription(SellListEmbed.description + "<@" + currentUserSell.id + "> " + currentUserSell.tag + ('\n'));
                            }
                        }
                        $countUserSell++
                        $countTotal = 0
                        currentUsersSell.forEach(userCurrent => {
                            $countTotal++
                        });
                        if ($countUserSell == $countTotal) {
                            message.channel.send(SellListEmbed);
                        }
                    }))).catch(console.error);
        }

    }

    if (message.content.substr(0, 14) == "?playerOshimo-") {

        message.channel.send("Le joueur " + message.author.tag)
        message.channel.send("Propose à l'échange")
        messageListToFetch = bot.guilds.cache.get($guildId).channels.cache.get($oshimoChannel).messages;
        let userExchange = ""
        let userSell = ""
        //Check the archimonstre for exchange
        function addExchange(archimonstre) {
            console.log("check exchange");
            userExchange = userExchange + archimonstre + "\n"
        }
        function addSell(archimonstre) {
            console.log("check exchange");
            userSell += archimonstre + "\n"
        }
        for (let i = 280; i < 287; i++) {
            // console.log("check" + i);
            var currentArchimonstre = '$archimonstreName' + i
            var currentMessage = '$messageOshimo' + i
            var currentMessageFetched = messageListToFetch.fetch(eval(currentMessage))


            // console.log(currentMessageFetched);
            // console.log(currentMessageFetched.then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $searchEmoji).users.fetch()));
            // currentMessageFetched.then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $searchEmoji)).then(result => console.log(result.users.resolve()));
            console.log("firstBefore", new Date());
            currentMessageFetched
                .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $searchEmoji).users.fetch()
                    .then(users => {
                        console.log("before", new Date());
                        console.log("checkFetchSearch" + i);
                        if (users.find(u => u.id == message.author.id)) {
                            console.log("after", new Date());
                            currentMessageFetched
                                .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $exchangeEmoji).users.fetch()
                                    .then(users => {
                                        console.log("afterExchange", new Date());
                                        if (users.find(u => u.username == message.content.substr(14))) {
                                            console.log("inExchange", new Date());
                                            addExchange(eval(currentArchimonstre));
                                        }
                                    }))
                            currentMessageFetched
                                .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $sellEmoji).users.fetch()
                                    .then(users => {
                                        console.log("afterSell", new Date());
                                        if (users.find(u => u.username == message.content.substr(14))) {
                                            addExchange(eval(currentArchimonstre));
                                            message.channel.send(userExchange)
                                            message.channel.send(eval(currentArchimonstre))
                                            message.channel.send(currentArchimonstre)
                                        }
                                    }))
                        }
                    }))
        }
        console.log("userExchange", userExchange);
        message.channel.send("échange")
        message.channel.send(userSell)

        // Check archimonstre for sell
        // message.channel.send("Vend")
        // for (let i = 1; i < 287; i++) {
        //     var currentArchimonstre = '$archimonstreName' + i
        //     var currentMessage = '$messageOshimo' + i
        //     bot.guilds.cache.get($guildId).channels.cache.get($oshimoChannel).messages.fetch(eval(currentMessage))
        //         .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $searchEmoji).users.fetch()
        //             .then(users => {
        //                 if (users.find(u => u.id == message.author.id)) {
        //                     bot.guilds.cache.get($guildId).channels.cache.get($oshimoChannel).messages.fetch(eval(currentMessage))
        //                         .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $sellEmoji).users.fetch()
        //                             .then(users => {
        //                                 if (users.find(u => u.username == message.content.substr(14))) {
        //                                     message.channel.send(eval(currentArchimonstre))
        //                                 }
        //                             }))
        //                 }
        //             }))
        // }





        // Check archimonstre searched

        // for (let i = 1; i < 287; i++) {
        //     var currentArchimonstre = '$archimonstreName' + i
        //     var currentMessage = '$messageOshimo' + i
        //     bot.guilds.cache.get($guildId).channels.cache.get($oshimoChannel).messages.fetch(eval(currentMessage))
        //         .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $searchEmoji).users.fetch()
        //             .then(users => {
        //                 message.channel.send("Recherche")
        //                 if (users.find(u => u.username == message.content.substr(14))) {
        //                     bot.guilds.cache.get($guildId).channels.cache.get($oshimoChannel).messages.fetch(eval(currentMessage))
        //                         .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $sellEmoji).users.fetch()
        //                             .then(users => {
        //                                 if (users.find(u => u.id == message.author.id)) {
        //                                     message.channel.send(eval(currentArchimonstre))
        //                                 } else {
        //                                     bot.guilds.cache.get($guildId).channels.cache.get($oshimoChannel).messages.fetch(eval(currentMessage))
        //                                         .then(messageFetch => messageFetch.reactions.cache.find(m => m._emoji.name == $exchangeEmoji).users.fetch()
        //                                             .then(users => {
        //                                                 if (users.find(u => u.id == message.author.id)) {
        //                                                     message.channel.send(eval(currentArchimonstre))
        //                                                 }
        //                                             }))
        //                                 }
        //                             }))

        //                 }
        //             }))
        // }


    }
})

bot.login('NzE5NDMzMTY1MDU1NDU5MzQ5.Xt3XKA.S-SpXtaOwL3jPQFLAxhtZG05rcI')